package traore.com.MavenTuto;

public class Facture {
	private String libelle;
	private Double montant = null;

	public Facture(String libelle, Double montant) {
		setLibelle(libelle);
		setMontant(montant);

	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

}
