package traore.com.MavenTuto;

import java.util.Arrays;
import java.util.List;

public class CalculStreamListe {

	

	public static double calculSomme(List<Facture> factures) {
		return factures.parallelStream()
				.mapToDouble(el -> {
					 if( el.getMontant()!=null) {
						return el.getMontant(); 
					 }
					 return 0;
				}
						).sum();
	}

	public static void main(String[] args) {
		List<Facture> listFactures = Arrays.asList(new Facture("f1", null), new Facture("f2", 40.0),
				new Facture("f3", 60.0));
		System.out.println("Hello World!" + CalculStreamListe.calculSomme(listFactures));
	}
}
